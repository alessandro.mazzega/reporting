USE Reporting

IF NOT(EXISTS(SELECT * FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME='City'))
BEGIN
create table City(
	CityId int PRIMARY KEY IDENTITY(1,1),
	[Name] nvarchar(MAX) NOT NULL,
	Latitude float NOT NULL,
	Longitude float NOT NULL,
	IsDeleted bit NOT NULL DEFAULT 0
	);
END

IF NOT(EXISTS(SELECT * FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME='Report'))
BEGIN
create table Report(
	ReportId int PRIMARY KEY IDENTITY(1,1),
	CityId int FOREIGN KEY REFERENCES City(CityId) NOT NULL,
	Quantity int NOT NULL DEFAULT 0,
	[Date] datetime2 NOT NULL,
	IsDeleted bit NOT NULL DEFAULT 0

	);
END