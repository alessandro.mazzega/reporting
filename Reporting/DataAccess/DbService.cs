﻿using Dapper;
using Reporting.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Reporting.DataAccess
{
    public class DbService : IDisposable, IDbService
    {
        private DbConnection _conn;
        private readonly string _connectionString;

        public DbService(string connectionString)
        {
            _connectionString = connectionString;
            GetConnection();
        }

        public DbConnection GetConnection()
        {
            _conn = new SqlConnection(_connectionString);
            _conn.Open();
            return _conn;
        }

        public void Dispose()
        {
            _conn.Dispose();
        }

        public async Task<List<City>> GetCities()
        {
            try
            {
                return (await _conn.QueryAsync<City>(@"SELECT * FROM City WHERE IsDeleted = 0")).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<City> GetCity(int cityId)
        {
            try
            {
                return await _conn.QueryFirstOrDefaultAsync<City>(@"SELECT * FROM City WHERE CityId = @cityId AND IsDeleted = 0", new { cityId });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<City> AddCity(City city)
        {
            try
            {
                city.CityId = await _conn.QueryFirstAsync<int>(@"INSERT INTO City VALUES (@name, @latitude, @longitude, 0) SELECT CAST(SCOPE_IDENTITY() as INT)", city);
                if (city.CityId == 0)
                {
                    return null;
                }
                return city;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Report>> GetReports()
        {
            try
            {
                return (await _conn.QueryAsync<Report>(@"SELECT * FROM Report WHERE IsDeleted = 0")).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Report> GetReport(int reportId)
        {
            try
            {
                return await _conn.QueryFirstOrDefaultAsync<Report>(@"SELECT * FROM Report WHERE ReportId = @reportId AND IsDeleted = 0", new { reportId });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Report> AddReport(Report report)
        {
            try
            {
                report.ReportId = await _conn.QueryFirstAsync<int>(@"INSERT INTO Report VALUES (@cityId, @quantity, GETDATE(), 0) SELECT CAST(SCOPE_IDENTITY() as INT)", report);
                if (report.ReportId == 0)
                {
                    return null;
                }
                return report;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> CleanDb()
        {
            try
            {
                return (await _conn.ExecuteAsync(@"DELETE FROM Report DBCC CHECKIDENT('Report',RESEED, 0) DELETE FROM City DBCC CHECKIDENT('City',RESEED, 0)")) > 0;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
