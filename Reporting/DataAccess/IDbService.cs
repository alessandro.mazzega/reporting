﻿using Reporting.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Reporting.DataAccess
{
    public interface IDbService
    {
        Task<City> AddCity(City city);
        Task<Report> AddReport(Report report);
        Task<bool> CleanDb();
        void Dispose();
        Task<List<City>> GetCities();
        Task<City> GetCity(int cityId);
        DbConnection GetConnection();
        Task<Report> GetReport(int reportId);
        Task<List<Report>> GetReports();
    }
}