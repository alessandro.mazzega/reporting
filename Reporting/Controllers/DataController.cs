﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reporting.DataAccess;
using Reporting.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Reporting.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private IDbService _db;
        public DataController(IDbService db)
        {
            _db = db;
        }

        [HttpGet("City")]
        public async Task<IActionResult> GetCities()
        {
            try
            {
                var res = await _db.GetCities();
                return res?.Count > 0 ? StatusCode(200, res) : StatusCode(404, new List<City>());
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("City/{id}")]
        public async Task<IActionResult> GetCity(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var res = await _db.GetCity(id);
                return res == null ? StatusCode(200, res) : StatusCode(404, null);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost("City")]
        public async Task<IActionResult> AddCity(City city)
        {
            try
            {
                if (city == null)
                {
                    return BadRequest();
                }
                var res = await _db.AddCity(city);
                return res != null ? StatusCode(200, res) : StatusCode(500, null);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("Report")]
        public async Task<IActionResult> GetReports()
        {
            try
            {
                var res = await _db.GetReports();
                return res?.Count > 0 ? StatusCode(200, res) : StatusCode(404, new List<Report>());
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("Report/{id}")]
        public async Task<IActionResult> GetReport(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var res = await _db.GetReport(id);
                return res == null ? StatusCode(200, res) : StatusCode(404, null);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost("Report")]
        public async Task<IActionResult> AddReport(Report report)
        {
            try
            {
                if (report == null)
                {
                    return BadRequest();
                }
                var res = await _db.AddReport(report);
                return res != null ? StatusCode(200, res) : StatusCode(500, null);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
