﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Reporting.Models;
using Reporting.DataAccess;
using Microsoft.EntityFrameworkCore;
using MoreLinq;

namespace Reporting.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDbService _db;
        public HomeController(ILogger<HomeController> logger, IDbService db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            var data = new DataModel();
            data.Cities = await _db.GetCities();
            data.Reports = await _db.GetReports();
            return View(data);
        }

        public async Task<IActionResult> Data()
        {
            var data = new DataModel();
            data.Cities = await _db.GetCities();
            data.Reports = await _db.GetReports();
            return View(data);
        }

        public async Task<IActionResult> Destroy()
        {
            await _db.CleanDb();
            return RedirectToAction("Data");
        }

        public async Task<IActionResult> Map()
        {
            var cities = await _db.GetCities();
            var reports = await _db.GetReports();
            var data = reports.Select(x => new MapModel { ReportId = x.ReportId, CityId = x.CityId, Quantity = x.Quantity, Date = x.Date, City = cities.FirstOrDefault(y => y.CityId == x.CityId) }).ToList();
            return View(data);
        }

        public async Task<IActionResult> Chart()
        {
            int days = 31;
            var cities = await _db.GetCities();
            var reports = await _db.GetReports();
            var data = new List<ChartModel>();
            var lowerLimit = DateTime.Today.Subtract(new TimeSpan(days, 0, 0, 0)).Date;
            var recentReports = reports.Where(x => x.Date.Date >= lowerLimit).GroupBy(x => x.CityId);
            foreach (var group in recentReports)
            {
                var city = cities.FirstOrDefault(x => x.CityId == group.Key);

                var entry = new ChartModel { CityId = city.CityId, Name = city.Name, Latitude = city.Latitude, Longitude = city.Longitude, Values = new List<int>() };
                for (int i = 1; i <= days; i++)
                {
                    entry.Values.Add(group.Where(x => x.Date.Date == lowerLimit.AddDays(i)).Sum(x => x.Quantity));
                }
                data.Add(entry);
            }
            return View(data);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
