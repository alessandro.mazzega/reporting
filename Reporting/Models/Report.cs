﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reporting.Models
{
    public class Report
    {
        public int ReportId { get; set; }
        public int CityId { get; set; }
        public int Quantity { get; set; }
        public DateTime Date { get; set; }
    }
}
