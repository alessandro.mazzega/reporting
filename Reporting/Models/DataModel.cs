using System;
using System.Collections.Generic;

namespace Reporting.Models
{
    public class DataModel
    {
        public List<City> Cities { get; set; }
        public List<Report> Reports { get; set; }
    }
}
